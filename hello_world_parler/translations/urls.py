from django.urls import path
from . import views

urlpatterns = [
    path("book-list/", views.book_list, name="book-list"),
    path("search-books/", views.search_list, name="search-list"),
    path("search/", views.search_bar, name="search"),
    path("form_book/", views.show_book_form, name="form"),
    path("create_book/", views.create_book, name="create-book"),
]