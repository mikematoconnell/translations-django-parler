from django.db import models
from parler.models import TranslatableModel, TranslatedFields

class Book(TranslatableModel):
    translations = TranslatedFields(
        title = models.CharField(max_length=40),
        description = models.TextField()
    ) 




