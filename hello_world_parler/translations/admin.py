from django.contrib import admin
from parler.admin import TranslatableAdmin
from .models import Book

class BookAdmin(TranslatableAdmin):
    list_display = ('title', 'description')
    fieldsets = (
        (None, {
            'fields': ('title', 'description'),
        }),
    )

admin.site.register(Book, BookAdmin)