from django.shortcuts import render
from .models import Book
from django.conf import settings

from django.shortcuts import redirect

# # Create your views here.
def create_book(request):
    input_values = request.GET
    set_lang = settings.PARLER_LANGUAGES
    set_languages = set_lang[None]
    book = Book.objects.language('en').create(
            title=input_values["title_en"],
            description=input_values["description_en"]
        )
    
    for language in set_languages:
        language_code = language["code"]
        book.set_current_language(language_code)
        title = input_values[f"title_{language_code}"]
        description = input_values[f"description_{language_code}"]
        if len(title) > 0:
            book.title = title
        if len(description) > 0:
            book.description = description
    
    book.save()

    return redirect("book-list")

def show_book_form(request):
    languages = []
    context = {}

    set_lang = settings.PARLER_LANGUAGES
    default_language = set_lang["default"]["fallbacks"][0]

    for item in set_lang[None]:
        languages.append(item["code"])

    context["language_list"] = languages
    context["default_language"] = default_language
    return render(request, "create.html", context)

# Create your views here.
def book_list(request):
    books = Book.objects.all()
    print(request.LANGUAGE_CODE) 

    context = {"book_list": books}
    return render(request, "book_list.html", context)

def search_list(request):
    user_language = request.LANGUAGE_CODE
    query = request.GET.get('q', '')  
    books = Book.objects.filter(
        translations__title__icontains=query,
        translations__language_code=user_language
    )  

    context = {"book_list": books}
    return render(request, "book_list.html", context)

def search_bar(request):
    return render(request, "search.html")